import argparse
from pyhive import hive
import json
import jsonschema


schema = {
    "type": "array",
    "title": "JSON Schema for Hive table",
    "description": "JSON Schema for Hive table",
    "default": [],
    "examples": [
        [
            {
                "colName": "empo",
                "colType": "INT"
            },
            {
                "colName": "ename",
                "colType": "STRING"
            }
        ]
    ],
    "additionalItems": False,
    "items": {
        "anyOf": [
            {
                "$id": "#/items/anyOf/0",
                "type": "object",
                "title": "The first anyOf schema",
                "description": "Column",
                "default": {},
                "examples": [
                    {
                        "colName": "empo",
                        "colType": "INT"
                    }
                ],
                "required": [
                    "colName",
                    "colType"
                ],
                "additionalProperties": False,
                "properties": {
                    "colName": {
                        "$id": "#/items/anyOf/0/properties/colName",
                        "type": "string",
                        "title": "The colName schema",
                        "description": "An explanation about the purpose of this instance.",
                        "default": "",
                        "examples": [
                            "empo"
                        ]
                    },
                    "colType": {
                        "$id": "#/items/anyOf/0/properties/colType",
                        "type": "string",
                        "title": "The colType schema",
                        "description": "An explanation about the purpose of this instance.",
                        "default": "",
                        "examples": [
                            "INT"
                        ]
                    }
                }
            }
        ]
    }
}


def table_schema(jsonSchema):
    data  = json.loads(jsonSchema)
    try:
        data  = json.loads(jsonSchema)
        jsonschema.validate(data, schema)
    except jsonschema.exceptions.ValidationError as e:
        raise Exception("well-formed but invalid JSON: " + repr(e))
    except json.decoder.JSONDecodeError as e:
        raise Exception("poorly-formed text, not JSON:" + repr(e))
    output = "("
    for i in range(len(data)):
        output += data[i]["colName"] + " " + data[i]["colType"]
        if i != len(data) - 1:
            output += ","
    output += ")"
    return output


def schema_map(jsonSchema):
    data  = json.loads(jsonSchema)
    try:
        data  = json.loads(jsonSchema)
        jsonschema.validate(data, schema)
    except jsonschema.exceptions.ValidationError as e:
        raise Exception("well-formed but invalid JSON: " + repr(e))
    except json.decoder.JSONDecodeError as e:
        raise Exception("poorly-formed text, not JSON:" + repr(e))
    output = ":key,"
    for i in range(1, len(data)):
        output += "cf:" + data[i]["colName"]
        if i != len(data) - 1:
            output += ","
    return output


def main(args):
    cursor = hive.connect(host= args.hive_host, port=args.hive_port).cursor()
    sql = r'''
    CREATE TABLE IF NOT EXISTS ''' + args.hive_hbase_table + " " + table_schema(args.schema) + " " + '''STORED BY 'org.apache.hadoop.hive.hbase.HBaseStorageHandler'
    WITH SERDEPROPERTIES ("hbase.columns.mapping" = "''' +  schema_map(args.schema) + '''")
    TBLPROPERTIES ("hbase.table.name" = "'''+ args.hbase_table + '''")
    '''
    cursor.execute(sql)
    
    
    sql2 = r'''
    INSERT OVERWRITE TABLE ''' + args.hive_hbase_table + ''' SELECT * FROM ''' + args.hive_table
    cursor.execute(sql2)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Hive to Hbase")
    parser.add_argument('-ho', dest="hive_host", required=True,
                        help="Hive host (host)")
    parser.add_argument('-p', dest="hive_port", required=True,
                        help="Hive port")
    parser.add_argument('-t', dest="hive_hbase_table", required=True,
                        help="Hive-Hbase table name")
    parser.add_argument('-s', dest="schema", required=True,
                        help="Table schema as a json array in single quotes")
    parser.add_argument('-hbt', dest="hbase_table", required=True,
                        help="Hbase table name")
    parser.add_argument('-hit', dest="hive_table", required=True,
                        help="Hive table name")
    main(parser.parse_args())

